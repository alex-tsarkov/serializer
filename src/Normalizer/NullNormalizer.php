<?php

namespace AlexTsarkov\Serializer\Normalizer;

final class NullNormalizer implements NormalizerInterface
{
    use NormalizerTrait;

    public function denormalize($normal, string $type = null)
    {
        return $normal;
    }

    protected function normalizeArray(array $value): array
    {
        return array_map([$this, 'normalize'], $value);
    }
}
