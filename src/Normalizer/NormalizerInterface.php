<?php

namespace AlexTsarkov\Serializer\Normalizer;

interface NormalizerInterface
{
    /**
     * Normalizes value before serialization. Must return null if resource given.
     *
     * @param mixed $value
     */
    public function normalize($value);

    /**
     * Denormalizes value after deserialization.
     *
     * @param mixed $normal
     */
    public function denormalize($normal, string $type = null);
}
