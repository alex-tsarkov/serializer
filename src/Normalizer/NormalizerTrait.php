<?php

namespace AlexTsarkov\Serializer\Normalizer;

trait NormalizerTrait
{
    public function normalize($value)
    {
        if (is_scalar($value)) {
            return $value;
        }
        if (is_array($value)) {
            return $this->normalizeArray($value);
        }
        if (is_object($value)) {
            return (object) $this->normalizeArray((array) $value);
        }

        return null;
    }

    abstract protected function normalizeArray(array $value): array;
}
