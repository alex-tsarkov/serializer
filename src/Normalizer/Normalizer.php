<?php

namespace AlexTsarkov\Serializer\Normalizer;

class Normalizer implements NormalizerInterface
{
    use NormalizerTrait;

    /**
     * @var \AlexTsarkov\Serializer\Normalizer\NormalizerInterface
     */
    private $nameNormalizer;

    public function __construct(NormalizerInterface $nameNormalizer = null)
    {
        $this->nameNormalizer = $nameNormalizer ?? new NullNormalizer();
    }

    public function denormalize($normal, string $type = null)
    {
        if (is_array($normal)) {
            return $this->denormalizeArray($normal, $type);
        }
        if (is_object($normal)) {
            return (object) $this->denormalizeArray((array) $normal, $type);
        }

        return $normal;
    }

    protected function skipItem($item, $key): bool
    {
        return false;
    }

    private function normalizeArray(array $value): array
    {
        $normal = [];
        foreach ($value as $key => $item) {
            if ($this->skipItem($item, $key)) {
                continue;
            }
            if (is_string($key)) {
                $key = $this->nameNormalizer->normalize($key);
                $normal[$key] = $this->normalize($item);
            } else {
                $normal[] = $this->normalize($item);
            }
        }

        return $normal;
    }

    private function denormalizeArray(array $normal, string $type = null): array
    {
        $value = [];
        foreach ($normal as $key => $item) {
            if (is_string($key)) {
                $key = $this->nameNormalizer->denormalize($key, 'string');
            }
            $value[$key] = $this->denormalize($item, $type);
        }

        return $value;
    }
}
