<?php

namespace AlexTsarkov\Serializer;

use AlexTsarkov\Serializer\Normalizer\NormalizerInterface;
use AlexTsarkov\Serializer\Normalizer\NullNormalizer;

class JsonSerializer implements SerializerInterface
{
    private $normalizer;

    public function __construct(NormalizerInterface $normalizer = null)
    {
        if (!extension_loaded('json')) {
            throw new \LogicException("Extension 'json' not loaded");
        }

        $this->normalizer = $normalizer ?? new NullNormalizer();
    }

    public static function encode($value): string
    {
        $options = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRESERVE_ZERO_FRACTION;
        $data = json_encode($value, $options);
        if (false === $data) {
            throw new \JsonException(json_last_error_msg(), json_last_error());
        }

        return $data;
    }

    public function serialize($value): string
    {
        return self::encode($this->normalizer->normalize($value));
    }

    public function deserialize(string $data, string $type = null)
    {
        $value = json_decode($data);
        $errno = json_last_error();
        if (JSON_ERROR_NONE !== $errno) {
            throw new \JsonException(json_last_error_msg(), $errno);
        }

        return $this->normalizer->denormalize($value, $type);
    }
}
