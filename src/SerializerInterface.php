<?php

namespace AlexTsarkov\Serializer;

interface SerializerInterface
{
    public function serialize($value): string;

    public function deserialize(string $data, string $type = null);
}
