<?php

namespace AlexTsarkov\Serializer\Normalizer;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Serializer\Normalizer\NullNormalizer
 */
class NullNormalizerTest extends TestCase
{
    use NormalizerTestTrait;

    protected function setUp(): void
    {
        $this->normalizer = new NullNormalizer();
    }

    /**
     * @dataProvider dataProvider
     *
     * @param mixed $expected
     */
    public function testDenormalize($expected, string $type = null): void
    {
        $actual = $this->normalizer->denormalize($expected, $type);
        if (is_numeric($actual) || !is_scalar($actual)) {
            $this->assertEquals($expected, $actual);
        } else {
            $this->assertSame($expected, $actual);
        }
    }

    public function dataProvider(): iterable
    {
        yield [null, null];
        yield [false, 'bool'];
        yield [true, 'bool'];
        yield [0, 'int'];
        yield [0.0, 'float'];
        yield ['', 'string'];
        yield [[], 'array'];
        yield [new \stdClass(), 'object'];
    }
}
