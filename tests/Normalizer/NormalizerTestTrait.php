<?php

namespace AlexTsarkov\Serializer\Normalizer;

trait NormalizerTestTrait
{
    /**
     * @var \AlexTsarkov\Serializer\Normalizer\NormalizerInterface
     */
    protected $normalizer;

    public function testNormalizeNull(): void
    {
        $this->assertNull($this->normalizer->normalize(null));
    }

    public function testNormalizeBool(): void
    {
        $this->assertFalse($this->normalizer->normalize(false));
        $this->assertTrue($this->normalizer->normalize(true));
    }

    /**
     * @testWith [-2147483648]
     *           [0]
     *           [2147483647]
     *           [-0.0]
     *           [0.0]
     *           [0.00001]
     *           [3.14159]
     *
     * @param float|int $expected
     */
    public function testNormalizeNumeric($expected): void
    {
        $this->assertSame($expected, $this->normalizer->normalize($expected));
    }

    /**
     * @testWith [""]
     *           ["The quick brown fox jumps over the lazy dog"]
     */
    public function testNormalizeString(string $expected): void
    {
        $this->assertSame($expected, $this->normalizer->normalize($expected));
    }

    public function testNormalizeResource(): void
    {
        $resource = fopen('php://memory', 'r+');
        if (false === $resource) {
            $this->markTestIncomplete('Failed to create resource');
        }
        $this->assertNull($this->normalizer->normalize($resource));

        if (false === fclose($resource)) {
            $this->markTestIncomplete('Failed to close resource');
        }
        $this->assertNull($this->normalizer->normalize($resource));
    }

    /**
     * @testWith [[]]
     *           [[null, true, 0, 1.0, ""]]
     *           [{"null": null, "bool": false, "int": 1, "float": 0.0, "string": "Az"}]
     */
    public function testNormalizeArray(array $expected): void
    {
        $this->assertSame($expected, $this->normalizer->normalize($expected));
    }

    /**
     * @testWith [{}]
     *           [{"a": 0, "b": 1}]
     */
    public function testNormalizeObject(array $expected): void
    {
        settype($expected, 'object');
        $this->assertEquals($expected, $this->normalizer->normalize($expected));
    }
}
