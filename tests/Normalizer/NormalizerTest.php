<?php

namespace AlexTsarkov\Serializer\Normalizer;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Serializer\Normalizer\Normalizer
 */
class NormalizerTest extends TestCase
{
    use NormalizerTestTrait;

    protected function setUp(): void
    {
        $this->normalizer = new Normalizer();
    }

    /**
     * @dataProvider dataProvider
     *
     * @param mixed $expected
     * @param mixed $value
     */
    public function testDenormalize($expected, $value, string $type = null): void
    {
        $actual = $this->normalizer->denormalize($value, $type);
        if (is_numeric($actual) || !is_scalar($actual)) {
            $this->assertEquals($expected, $actual);
        } else {
            $this->assertSame($expected, $actual);
        }
    }

    public function testSkipItem(): void
    {
        $this->normalizer = new class() extends Normalizer {
            protected function skipItem($item, $key): bool
            {
                return 0 !== $key % 2;
            }
        };

        $this->assertSame([1, 3, 5], $this->normalizer->normalize([1, 2, 3, 4, 5]));
    }

    public function testNameNormalizer(): void
    {
        $nameNormalizer = $this->createMock(NormalizerInterface::class);
        $nameNormalizer->method('normalize')
            ->will($this->returnCallback(static function ($value) {
                return strtoupper($value);
            }))
        ;
        $nameNormalizer->method('denormalize')
            ->will($this->returnCallback(static function ($value, string $type = null) {
                return strtolower($value);
            }))
        ;

        $this->normalizer = new Normalizer($nameNormalizer);

        $value = ['Az' => 'Az'];
        $this->assertSame(['AZ' => 'Az'], $this->normalizer->normalize($value));
        $this->assertSame(['az' => 'Az'], $this->normalizer->denormalize($value));
    }

    public function dataProvider(): iterable
    {
        yield [null, null, null];
        yield [[1, 2, 3], [1, 2, 3], 'array'];
        yield [new \stdClass(), new \stdClass(), 'object'];
    }
}
