<?php

namespace AlexTsarkov\Serializer;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \AlexTsarkov\Serializer\JsonSerializer
 */
class JsonSerializerTest extends TestCase
{
    protected $serializer;

    protected function setUp(): void
    {
        if (!extension_loaded('json')) {
            $this->markTestSkipped('The JSON extension is not available.');
        }

        $this->serializer = new JsonSerializer();
    }

    public function testSerializeNull(): string
    {
        $actual = $this->serializer->serialize(null);
        $this->assertSame('null', $actual);

        return $actual;
    }

    /**
     * @testWith [false, "false"]
     *           [true, "true"]
     *           [-2147483648, "-2147483648"]
     *           [0, "0"]
     *           [2147483647, "2147483647"]
     *           [-0.0, "-0.0"]
     *           [0.0, "0.0"]
     *           [0.00001, "1.0e-5"]
     *           [3.14159, "3.14159"]
     *           ["", "\"\""]
     *           ["The quick brown fox jumps over the lazy dog", "\"The quick brown fox jumps over the lazy dog\""]
     *
     * @param mixed $scalar
     */
    public function testSerializeScalar($scalar, string $expected): void
    {
        $this->assertSame($expected, $this->serializer->serialize($scalar));
    }

    /**
     * @depends testSerializeNull
     */
    public function testSerializeResource(string $expected): void
    {
        $resource = fopen('php://memory', 'r+');
        if (false === $resource) {
            $this->markTestIncomplete('Failed to create resource');
        }
        $this->assertSame($expected, $this->serializer->serialize($resource));

        if (false === fclose($resource)) {
            $this->markTestIncomplete('Failed to close resource');
        }
        $this->assertSame($expected, $this->serializer->serialize($resource));
    }

    /**
     * @testWith [[], "[]"]
     */
    public function testSerializeArray(array $array, string $expected): void
    {
        $this->assertSame($expected, $this->serializer->serialize($array));
    }

    /**
     * @testWith [{}, "{}"]
     */
    public function testSerializeObject(array $value, string $expected): void
    {
        $object = (object) $value;
        $this->assertSame($expected, $this->serializer->serialize($object));
    }

    /**
     * @dataProvider dataProvider
     *
     * @param mixed $expected
     */
    public function testDeserialize($expected, string $data, string $type = null): void
    {
        $actual = $this->serializer->deserialize($data, $type);
        if (is_numeric($actual) || !is_scalar($actual)) {
            $this->assertEquals($expected, $actual);
        } else {
            $this->assertSame($expected, $actual);
        }
    }

    public function dataProvider(): iterable
    {
        yield [null, 'null', null];
        yield [false, 'false', 'bool'];
        yield [true, 'true', 'bool'];
        yield [0, '0', 'int'];
        yield [0.0, '0', 'float'];
        yield ['', '""', 'string'];
        yield [[], '[]', 'array'];
        yield [new \stdClass(), '{}', 'object'];
    }
}
